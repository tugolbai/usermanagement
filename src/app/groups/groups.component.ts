import { Component, Input, OnInit } from '@angular/core';
import { Group } from '../shared/group.model';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  @Input() groups!: Group[];

  constructor(private groupService: GroupService) {
  }

  ngOnInit() {
    this.groups = this.groupService.getGroups();
    this.groupService.groupItemsChange.subscribe((groups: Group[]) => {
      this.groups = groups;
    });
  }

  onClick(index: number) {
    this.groups.forEach(gr => {
      gr.value = false;
    });
    this.groups[index].value = true;
  }


}
