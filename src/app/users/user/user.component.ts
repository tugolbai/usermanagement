import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../shared/user.model';
import { GroupService } from '../../shared/group.service';
import { Group } from '../../shared/group.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit{
  @Input() user!: User;
  groups!: Group[];

  constructor(private groupService: GroupService) {
  }

  ngOnInit() {
    this.groups = this.groupService.getGroups();
    this.groupService.groupItemsChange.subscribe((groups: Group[]) => {
      this.groups = groups;
    });
  }

  check() {
    if (this.user.active) {
      return 'Yes';
    } else {
      return 'No';
    }
  }

  onClick() {
    this.groups.forEach(gr => {
      if (gr.value) {
        if (!(gr.usersArr.includes(this.user))) {
          gr.usersArr.push(this.user);
        }
      }
    })
  }

}
