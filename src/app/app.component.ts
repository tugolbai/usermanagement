import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showComponent = '';
  showCreateComponent() {
    this.showComponent = 'manage';
  }
  showUsersAndGroupsComponent() {
    this.showComponent = 'userGroup';
  }
}
