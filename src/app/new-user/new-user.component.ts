import { Component, ElementRef, ViewChild } from '@angular/core';
import { User } from '../shared/user.model';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent  {
  active = false;
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('activeInput') activeInput!: ElementRef;
  @ViewChild('roleSelect') roleSelect!: ElementRef;

  constructor(public userService: UserService) { }

  createUser() {
    this.active = false;
    const name = this.nameInput.nativeElement.value;
    const email = this.emailInput.nativeElement.value;
    const active = this.activeInput.nativeElement;
    const role = this.roleSelect.nativeElement.value;
    if (name === '' || email === '' || role === 'Role') {
      alert('Заполните все поля!');
    } else {
      if (active.checked) {
        this.active = true;
      }
      const user = new User(name, email, this.active, role);
      this.userService.addUser(user);
    }
  }
}
