import { User } from './user.model';

export class Group {
  constructor(
    public name: string,
    public value: boolean = false,
  ) {}
  usersArr: User[] = [];
}
