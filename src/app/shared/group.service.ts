import { Group } from './group.model';
import { EventEmitter } from '@angular/core';

export class GroupService {
  groupItemsChange = new EventEmitter<Group[]>();

  private groups: Group[] = [
    new Group('Hiking group'),
    new Group('Book club')
  ];

  getGroups() {
    return this.groups.slice();
  }

  addGroup(group: Group) {
    this.groups.push(group);
    this.groupItemsChange.emit(this.groups);
  }
}
