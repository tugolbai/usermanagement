import { User } from './user.model';
import { EventEmitter } from '@angular/core';

export class UserService {
  usersChange = new EventEmitter<User[]>();

  private users: User[] = [
    new User('Tugolbai',  '111@gmail.com', true , 'admin'),
    new User('Tilek', '222@gmail.com', false , 'user'),
    new User('Aiperi', '333@gmail.com', true , 'editor'),
  ];

  getUsers() {
    return this.users.slice();
  }

  addUser(user: User) {
    this.users.push(user);
    this.usersChange.emit(this.users);
  }

}
