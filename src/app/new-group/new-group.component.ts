import { Component, ElementRef, ViewChild } from '@angular/core';
import { Group } from '../shared/group.model';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.css']
})
export class NewGroupComponent {
  @ViewChild('nameInput') nameInput!: ElementRef;

  constructor(public groupService: GroupService) { }

  createGroup() {
    const name = this.nameInput.nativeElement.value;
    if (name === '') {
      alert('Заполните название группы!');
    } else {
      const group = new Group(name);
      this.groupService.addGroup(group);
    }
  }
}
